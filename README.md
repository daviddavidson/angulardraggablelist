##Draggable list items
####A dead-simple demo of a draggable list with Angular / jQuery UI

Here it is in action: http://draggabletest.davidd.ziprtest.com/
and http://purview.davidd.ziprtest.com/

(You'll only need to npm install / Grunt if you're running this on localhost.)
